--- 
title: Border Radius Previewer
date: '2021-05-01T12:00:00.000Z'
description: "This article is second in series in my React Tips Blog and describes how ReactJS can be used in accordance with CSS to modify a box's border radius property."
---

## Table of Contents

---
&nbsp;

<ul>
    <li>
        <a href="#create-project">
            Creating the Project
        </a>
    </li>
    <li>
        <a href="#starting-modifications">
            Bootstrap and Styling
        </a>
    </li>
    <li>
        <a href="#main-process">
            Main process
        </a>
        <ul>
            <li>
                <a href="#react-state">
                    React State/Lifecycle
                </a>
            </li>
            <li>
                <a href="#demo-box">
                    Creating our demo box
                </a>
            </li>
            <li>
                <a href="#dom-manipulation">
                    Manipulating our SASS/CSS stylesheet using React
                </a>
            </li>
        </ul>
    </li>
</ul>

---
&nbsp;

#:star2: [Border Radius Previewer]()
&nbsp;

Hello, this article will examine implementing a border radius previewer feature using ReactJS.
The codes to implement this can be found [here](https://github.com/Jhesse-Jack/Border-Radius-Previewer)
&nbsp;

Let's get started.
&nbsp;
