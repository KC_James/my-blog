--- 
title: Binary to Decimal Conversion
date: '2021-05-08T12:00:00.000Z'
description: " This article describes how a Reactjs newbie can build a binary to decimal converter from scratch and understand some basic Reactjs concepts."
---

## Table of Contents

--- 
&nbsp;

<ul>
    <li>
        <a href="#create-project">
            Creating the Project
        </a>
    </li>
    <li>
        <a href="#starting-modifications">
            Bootstrap and Styling
        </a>
    </li>
    <li>
        <a href="#main-process">
            Main process
        </a>
        <ul>
            <li>
                <a href="#react-state">
                    React State/Lifecycle
                </a>
            </li>
            <li>
                <a href="#accept-input">
                    Accepting input for binary figures
                </a>
            </li>
            <li>
                <a href="#functions">
                    Functions
                </a>
            </li>
        </ul>
    </li>
        <li>
        <a href="#reactstrap-modal">
            Reactstrap Modal Boilerplate Component
        </a>
    </li>
    <li>
        <a href="#algorithm">
            Main Algorithm
        </a>
    </li>
</ul>

---
&nbsp;

#:star2: [Binary to Decimal Conversion Project](https://bin-dec.netlify.app)
&nbsp;

Hello, this article will examine implementing a binary to decimal conversion feature using ReactJS.
The codes to implement this can be found [here](https://github.com/Jhesse-Jack/Binary-to-Decimal)
&nbsp;

Let's get started.
&nbsp;

***Pre-requisites***
* Have node installed on your computer. 
* Basic understanding of JavaScript and some ES6 concepts. 
&nbsp;

<h1 id="create-project"> Creating the Project </h1>
---
So to get started with our React boilerplate template, we need to issue the command: [npx create-react-app (user-defined filename)](https://reactjs.org/docs/create-a-new-react-app.html). For my user-defined filename, I will use binary-to-decimal. Now, we `cd [project directory]` and `npm run start` / `npm start`.
After a few seconds/minutes after the development server has started, we should be able to see the page below on our browsers:

&nbsp;

> ![React Boilerplate](../../assets/Binary-to-Decimal Conversion/React Boilerplate.png)
---
&nbsp;

<h1 id="starting-modifications"> Bootstrap & Styling </h1>

Now, we navigate to **src/App.js** where we will make changes to the file to reflect on our local server development server.
In **src/App.js**, we should see this code:

&nbsp;

> ![Initial Boilerplate](../../assets/Binary-to-Decimal Conversion/Initial Boilerplate.png)
 &nbsp;

Steps:
* We will download [bootstrap.min.css](https://getbootstrap.com/docs/4.3/getting-started/download/) and import `bootstrap.min.css` in our App.js file.
* Alright, now we need to cater for the input box the user would input the binary text into. For that, we'll insert the following codes:
    * Wait, before that we need to clear all the codes in the `div className = {App header} `
    * Now, we add the following code:
    ```javascript
        <p>
          Enter any binary number (to be converted to decimal base): &nbsp;
          <input
            type="number"
            className="form-control"
            placeholder="Enter any binary number"
            title="Binary numbers only"
          />
        </p>

<h1 id="#main-process"> Main Process </h1>
    <h2 id="#react-state"> React State/Lifecycle </h2>

---
We now need to define a variable to hold the input value from the user. We will use React's **useState** function for this so the input can change dynamically.
Steps:
* `import React, { useState } from 'react'`
* Add a ` let [binaryNum, setbinaryNum] = React.useState('')`
    * This statement means we are defining a variable called binaryNum and it's set by calling a variable (i.e setbinaryNum); we are initializing this value to an empty string.
* Inside our **` <input />`** tag, we add a value property/attribute.
    * ` value={binaryNum}`

    <h2 id="#accept-input"> Accepting input for binary figures </h2>

---
To truly utilise the user input value, we need to define an onChange or onInput property for the input tag. We will use the latter in this article.
Steps:
* Define onInput property for input tag
    * `onInput = {handleNumChange}`
    where handleNumChange is our function to handle whatever changes has been made to the input box by the user.
* Let's define what goes into our `handleNumChange` function next.
     ```javascript
     const [isValid, setisValid] = useState('false')
      const handleNumChange = (e) => {
          const regexp = /^[0-1]*[:.,-]?$/g;
          if (regexp.test(e.target.value)) {
              setisValid(true);
              setbinaryNum(e.target.value);
            } 
          else {
            setisValid(false);
            }
        };
* #:smiley: we will delve into the details of the function next
* This is what our function defined above is trying to achieve
    * First off, users will always try to enter a number which is not binary even though we have pre-informed them with the help of the placeholder and title attributes of the input tag. There are a couple of ways we can treat this: 
        * We can accept the inputs and filter out only binary inputs or 
        * We can only accept binary inputs and make sure all non-binary inputs are numb/ not accepted in the input box.
    * In the function above, we are opting for the latter option. To do this, we need the help of the much dreaded regular expressions.
    The regular expression `/^[0-1]*[:.,-]?$/g` means we are only accepting figures between the range of o to 1 and just those two numbers multiple times because they are the only numbers associated with binary numeral system.
    > Having trouble studying regular expressions? [Check this out](https://blog.bitsrc.io/a-beginners-guide-to-regular-expressions-regex-in-javascript-9c58feb27eb4).
    &nbsp;
    You can test your regular expressions written [here](https://regexr.com/) as well.
    * `regexp.text` is used as a function here to check if indeed the user input value matches the regular expressions pattern defined. If indeed they match then, we set the number to the variable using `setbinaryNum`.

* Let's discuss our `setisValid` attribute in the function now. The `isValid` attribute will be used to check if the input is actually inputting a binary/ non-binary input and display a warning to the user. It's initialised to `false` to prompt the user when they start entering the value, however when our user enters a binary input it's set to `true` to disappear from under our text box.
    ```javascript
          {!isValid ? (
            <p style={{ color: "red", fontSize: "12px", textAlign: "left" }}>
              Input a Binary Number
            </p>
          ) : null}
A ternary operator is used to check whether the input value is valid or not.
Basically, the function above is placed next after our input tag. 
This statement checks if the user has entered an input value using the regular expression defined and the set value for the binaryNum, if the user entered an invalid literal/numeral, the first part of the ternary condition is executed otherwise nothing is displayed and the user can go on with their input.

<h2 id="#reactstrap-modal"> Reactstrap Modal Boilerplate Component </h2>

---
For this section, we'll start by creating a separate component which we'll import in our main <code>App.js</code> file.
* Let's call it <code>Modal.js</code>.
* Now we need to install react-bootstrap by running <code>npm install react-bootstrap bootstrap</code>
* Now, we'll be using the Button and Modal components of react-bootstrap to display our answer to the user using this code block:
    ```javascript
    import React from "react";
    import "../bootstrap.min.css";
    import "../App.css";
    import { Button, Modal } from "react-bootstrap";

    const ResultModal = (props) => {
    return (
        <>
        <Modal show={props.show} onHide={props.close}>
            <Modal.Header closeButton>
            <Modal.Title> Binary Converter </Modal.Title>
            </Modal.Header>
            <Modal.Body>Your answer is: {props.result}</Modal.Body>
            <Modal.Footer>
            <Button variant="secondary" onClick={props.close}>
                Close
            </Button>
            </Modal.Footer>
        </Modal>
        </>
    );
    };

    export default ResultModal;

P.S: If you were wondering, our answer would be passed to this component using props.
*   Let's return back to our <code>App.js</code> file to define what properties should be passed on to the <code>Modal.js</code> component.
We need to define values for show, close and result because they are being passed as props if you observe from the code above.
    ```javascript
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const [decimal, setDecimal] = useState(null);
* Before we continue, if you noticed, we do not have a button for the user to hit on before the conversion is done. Let's do that now.
    ```javascript
    <Button variant="primary" onClick={handleShow}>
        CONVERT
    </Button>
* Let's now define our handleShow function which will handle what happens when the user hits the conversion button.
    ```javascript
    const handleShow = () => {
        setShow(true);
        setDecimal(convertToDecimal());
        clearInput();
    };

<h2 id="#algorithm"> Binary to Decimal Algorithm </h2>

---
* The <code>converttoDecimal()</code> function will contain the algorithm for changing the binary number.
* At this point we should have catered for all other factors.
* We can implement the algorithm in two ways either:
    * use JavaScript's inbuilt function (<code>parseInt(our parameter will be the binary number we are converting and the numeral base we are converting so our params will be binaryNum and 2 (binaryNum, 2))</code>)
    * use our own defined algorithm to handle that:
        ```javascript
        let summation = 0
        let i = 0
        while (binaryNum){
            summation= summation + Math.pow(2, i) * (binaryNum % 10);
            i++;
            binaryNum = parseInt(binaryNum/10);
            console.log(summation)
        }
    It's obvious i is our increment counter and summation is our variable to save our final answer.

Send me what you think about the algorithm.

And that will be all for this article.

---
Congrats #:tada: you have successfully implemented a binary to decimal feature using some key React concepts. 

