---
title: Binary to Decimal Conversion
date: "2021-04-02T12:00:00.000Z"
description: " This article describes how a Reactjs newbie can build a binary to decimal converter from scratch and understand some basic Reactjs concepts."
---

### Table of Contents

---
&nbsp;

<style>
</style>

<ul class="TOC" type="i">
    <li> 
        <a href="#createproject">
            Creating the Project 
        </a>
    </li>
    <li> 
        <a href="#makingmods">
            Setting up Bootstrap/Styling
        </a>
    </li>
    <li> 
        <a href="#reactstate"> 
            React State/Lifecycle 
        <a>
    </li>
        <li> 
        <a href="#conversionalgo"> 
            Binary Conversion Algorithm
        <a>
    </li>
        <li> 
        <a href=""> 
            Reactstrap Modal Boilerplate
        <a>
    </li>
        <li> 
        <a href=""> 
            Destructuring into components 
        <a>
    </li>
</ul>

---
<br />

<section id="createproject">
    Hello, this article will examine implementing a binary to decimal conversion 
    <br /> feature in ReactJS.
    The codes to implement this can be found <a href="https://github.com/Jhesse-Jack/Binary-to-Decimal"> here<a>.
    &nbsp;
    <br />
    Let's get started.
    <br />
    &nbsp;
    <b><i>Pre-requisites</i></b>
    <ul class="prereq">
        <li> Have node installed on your computer. </li>
        <li> Basic understanding of JavaScript and some ES6 concepts. </li>
    </ul>
    So to get started we need to set up our React boilerplate template, we can do that 
    by issuing the command 
        <a href="https://reactjs.org/docs/create-a-new-react-app.html">npx run create-react-app react-app </a>. 
        The last word <b>react-app</b> is going to be the name of our project. 
    Right, we have got our project locally on our computer, run <mark>cd react-app</mark> to advance into 
    the directory of your project. 
    Issue the command <mark>npm start/ npm run start</mark> to start the application locally once in the directory.
    When the application is loaded, you should see the browser open the page as shown below.
    &nbsp;
    <img src="../../assets/Binary-to-Decimal Conversion/React Boilerplate.png">
</section>

---
&nbsp;
&nbsp;

<section id="makingmods">
    Alright now we can navigate to <b>src/App.js</b> where all the changes will be made.
    In <b>src/App.js</b> we should see this code:
    &nbsp;
    <br />
    <img src="../../assets/Binary-to-Decimal Conversion/Initial Boilerplate.png">
    <br />
    Now, we will edit the codes enclosed in the <mark>header tag</mark> and then import bootstrap and use some react-libraries.
    First off we will make some modifications to our <mark>div with classname App-header</mark>, we'll add these lines:
    <br />
    <img src="../../assets/Binary-to-Decimal Conversion/Binary Numeral Input.png">
    <br />
    Because of the code we just added and the fact that it is JSX, we will need to <mark>import React from 'react' </mark>
    We should now see this on our webpage:
    &nbsp;
    <img src="../../assets/Binary-to-Decimal Conversion/Aftermath of Binary Numeral Input.png">
</section>

---
&nbsp;
&nbsp;

<section id="reactstate">
    At this point, it's now time to add an <mark> onInput</mark> property to the input placeholder so we can define a function to work on the inputs defined by the user. 
    We'll be adding <code> onInput = {handleNumbChange} </code> to our code now.</code> 
    But wait, <mark> handleNumbChange</mark> isn't defined. Let's go ahead and define it now. We will define it as shown below: 
    &nbsp;
    <br />
    <img src="../../assets/Binary-to-Decimal Conversion/handleNumbChange.png">
    <br />
    Wait, if you look at the code carefully you realise we have introduced a new variable called <mark>setDecnum </mark>. Let's go ahead and define that variable which will be the variable we receive from the user.
    To handle the variable from the user, we will define a lifecycle variable to be able to update at any point. Since we are using a functional/classless component, we will use have to <code> import {useState} from 'react'</code>.
    Now, after we declare the variable like below:
    <br />
    <code><mark> let [decnum, setDecnum] = useState("") </mark></code>
    <br />
    Next up, we will look at the algorithm to handle the input from the user and give us our desired conversion.
</section>

<section id="conversionalgo">
    How do we usually convert binary numbers to decimal traditionally? The digits in the binary number are multiplied by 2 raise to the power the position of the digit in the binary number and then summed. For instance:
    <blockquote>
        101011<sub>2</sub> = (1 x 2<sup>0</sup>) + (1 x 2<sup>1</sup>) + (1 x 2<sup>3</sup>) + (1 x 2<sup>5</sup>) = 43<sub>10</sub>
    </blockquote>
    First of all, to do this: we need to initialize 2 variables:
    <mark> let i=0</mark> and <mark> let summation=0</mark>
    <blockquote> i will be our counter and summation will be the variable to store the final answer. </blockquote>
    Below is a snapshot of the algorithm we will use:
    <br />
    <img src="../../assets/Binary-to-Decimal Conversion/Algorithm.png">
</section>