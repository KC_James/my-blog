import React from 'react';
import { EmailShareButton, EmailIcon } from "react-share";
import { FacebookShareButton, FacebookIcon } from "react-share";
import { LinkedinShareButton, LinkedinIcon } from "react-share";
import { TelegramShareButton, TelegramIcon } from "react-share";
import { TwitterShareButton, TwitterIcon } from "react-share";
import { WhatsAppShareButton, WhatsappIcon } from "react-share";

const SocialMediaButtons = ({title, url, twitterHandle}) => {
    return (
        <div>
            {/* // <EmailShareButton>
            // </EmailShareButton> */}
            <FacebookShareButton
                url = {url}
                quote={"Share this article"}
                hashtag="#portfolio_site"
                // className={classes.SocialMediaButton}
                >
                <FacebookIcon size={30} round={true} />
            </FacebookShareButton>
            {/* // <LinkedinShareButton>
            // </LinkedinShareButton>
            // <TelegramShareButton>

            // </TelegramShareButton> */}
            <TwitterShareButton
                url = {url}
                title = {title}
                quote={"Share this article"} 
                via={twitterHandle}
                hashtags="#remedy-news">
                    <TwitterIcon size={20} round={true}/>
            </TwitterShareButton>
            {/* // <WhatsAppShareButton>

            // </WhatsAppShareButton> */}
        </div>
    )
}

export default SocialMediaButtons;