import React, { useEffect, useState } from 'react';
import { Link } from 'gatsby';
import '../bootstrap.min.css';
import '../colorflip.scss';
import Header from "./header";
//import BlogPost from "./BlogPost";
const Layout = ({ location, title, children }) => {
	const [ published, setPublished ] = useState(2021);
	const [ yearNow, setYearNow ] = useState(null);
	useEffect(() => {
		const today = new Date();
		const thisYear = today.getFullYear();
		//this.setState({yearNow})
		setYearNow(setYearNow);
	});

	const rootPath = `${__PATH_PREFIX__}/`;
	const isRootPath = location.pathname === rootPath;
	let header;

	if (isRootPath) {
		header = (
			<div className="row">
				<div className="col-sm-1" />
				<div className="col-sm-8">
					<h1 className="main-heading">
					</h1>
				</div>
				<div className="col-sm-3" />
			</div>
		);
	} else {
		header = (
			<div className="row">
				<div className="col-sm-3">
				</div>
				<div className="col-sm-8 text-center">
					<div className="col-sm-1" />
				</div>
			</div>
		);
	}

	return (
		<div>
			<Header />
			<div className="swapColor">
				<div
					style={{
						margin: '0 auto',
						maxWidth: 960,
						padding: `0 1.0875rem 1.45rem`,
					}}>

				</div>
				<div className="global-wrapper swapColorII" data-is-root-path={isRootPath}>
					<header className="global-header">{header}</header>
					<main>{children}</main>
					{/* <BlogPost /> */}
					<Link to="/comments/" style={{ textDecoration: 'none', color: '#000000' }}>
						Leave a comment for me
					</Link>
					&nbsp; &nbsp; &nbsp;
					<footer>
						<span>
							{' '}
							©
							{published}
						</span>{' '}
						{yearNow > 2021 ? <span> - {yearNow}</span> : null} Tips on ReactJS for beginners
						{` `}
						<p>Gatsby Powered</p>
					</footer>
				</div>
			</div>
		</div>
	);
};

export default Layout;


{/* <Link to="/"  className="text-decoration-none >
<img
	src={Logo}
	alt="Not Loading"
	style={{
	}}
/>
</Link> 
					<h1 className="main-heading">
						<Link
							to="/"
							className="text-decoration-none text-justify"
							style={{
								width: '160px',
								height: '100px',
								marginBottom: '10px',
								marginTop: '10px'
							}}
						>
							{title}
						</Link>
										</h1>
														<img
						src={Logo}
						alt="Not Loading"
						style={{
							width: '160px',
							height: '100px',
							marginBottom: '10px',
							marginTop: '10px'
						}}
					/>
				*/}