/**
 * Bio component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Image from "gatsby-image"
// import { 
//   AiOutlineTwitter, AiOutlineFacebook, AiOutlineInstagram, AiOutlineLinkedin 
// } from "react-icons/ai"
import {
  FaTwitter,
  FaLinkedin,
  FaGithub,
} from "react-icons/fa"
import { IconContext } from 'react-icons'
// import { Icon } from "@material-ui/core"
// import { AiOutlineFacebook } from "react-icons/ai"
// import { AiOutlineInstagram } from "react-icons/ai"
// import { AiOutlineLinkedin } from "react-icons/ai";

const Bio = () => {
  const data = useStaticQuery(graphql`
    query BioQuery {
      avatar: file(absolutePath: { regex: "/profile_pic.jpg/" }) {
        childImageSharp {
          fixed(width: 50, height: 50, quality: 95) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      site {
        siteMetadata {
          author {
            name
            summary
            website
          }
          social {
            twitter
            linkedin
            github
          }
        }
      }
    }
  `)

  // Set these values by editing "siteMetadata" in gatsby-config.js
  const author = data.site.siteMetadata?.author
  const social = data.site.siteMetadata?.social

  const avatar = data?.avatar?.childImageSharp?.fixed

  return (
    <div className="bio">
      {avatar && (
        <Image
          fixed={avatar}
          alt={author?.name || ``}
          className="bio-avatar"
          imgStyle={{
            borderRadius: `50%`,
          }}
        />
      )}
      {author?.name && (
        <p>
          Written by &nbsp;
          <a href={`${author?.website || ``}`}>
            <strong>{author.name}</strong> 
          </a> {author?.summary || null}
          {` `}
          <br />
          &nbsp;
          <h6>
            Follow me on: 
            &nbsp; 
            <IconContext.Provider value={{ className: "icn-twitter" }}>
              <a href={`https://twitter.com/${social?.twitter || ``}`} className="mx-2">
                <FaTwitter />
              </a>
            </IconContext.Provider>
            {` `}  
            <IconContext.Provider value={{ className: "icn-linkedin"}}>
              <a href={`https://linkedin.com/in/${social?.linkedin || ``}`} className="mx-2">
                <FaLinkedin />
              </a>
            </IconContext.Provider>
            {` `}  
            <IconContext.Provider value={{ className: "icn-github"}}>
              <a href={`https://github.com/${social?.github || ``}`} className="mx-2">
                <FaGithub />
              </a>
            </IconContext.Provider>
            {` `}
          </h6>
        </p>
      )}
    </div>
  )
}

export default Bio
