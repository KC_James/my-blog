// import React from 'react'
// import { graphql } from "gatsby"
// import SocialMediaButtons from "../components/share"

// export const query = graphql`
//     query {
//         markdownRemark {
//             frontmatter {
//                 title
//             }
//             html
//         }
//     }
// `
// const BlogPost = props => {
//     const title = `Read ${props.data.markdownRemark.frontmatter.title}`;
//     const url = props.location.href;
//     const twitterHandle = "KC_Jxmes";

//     return (
//         <div>
//             <div 
//                 className="post-body__content"
//                 dangerouslySetInnerHTML={{__html: props.data.markdownRemark.html}}>
//             </div>
//             <div>
//                 <SocialMediaButtons 
//                     title={title}
//                     url={url}
//                     twitterHandle={twitterHandle} />
//             </div>
//         </div>
//     )
// }

// export default BlogPost