import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import "./header.scss"

const Header = ({ siteTitle }) => (
    <header id="header" style={Styles.header} className="header">
        <nav className="left">
            <Link to="/" className="logo">
                <span>
                    React with James
                </span>
            </Link>
            {/* <a href="#" className="logo">
                <span>
                    KCJames UI
                </span>
            </a> */}
        </nav>
    </header>
)

Header.propTypes = {
    siteTitle: PropTypes.string,
}

Header.defaultProps = {
    siteTitle: ``,
}

const Styles = {
    header: {
        margin: '0%',
        padding: '0%',
        border: '0',
        fontSize: '100%',
        font: 'inherit',
        verticalAlign: 'baseline',
        display: 'block',
    }
};

export default Header
