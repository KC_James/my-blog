import React, { Component } from 'react';
import axios from 'axios';
import '../colorflip.scss';
import Header from "../components/header";
import '../bootstrap.min.css';

class UserForm extends Component {
	constructor() {
		super();
		this.state = {
			name: '',
			email: '',
			subject: '',
			message: ''
		};
	}

	onChange = (e) => {
		/*
      Because we named the inputs to match their
      corresponding values in state, it's
      super easy to update the state
    */
		this.setState({ [e.target.name]: e.target.value });
	};

	onSubmit = (e) => {
		e.preventDefault();
		// get our form data out of state
		const { name, email, subject, message } = this.state;

		axios.post('/', { name, email, subject, message }).then((result) => {
			//access the results here....
		});
	};

	render() {
		const { name, email, subject, message } = this.state;
		return (
			<div className=" comments swapColorII">
				<Header />
				<div className="row">
					<div className="col-md-3" />
					<div className="col-md-6">
						<div className="col-md-12 mt-4">
							<h4 className="text-center">
								<strong>Send me a message</strong>
							</h4>
						</div>
						<form 
							method="POST" 
							id="message-form" 
							onSubmit={this.onSubmit} 
							action="mailto:jamesarmah79@gmail.com" >
							<div className="row mt-5">
								<div className="col-md-6 form-group">
									<input
										type="text"
										className="form-control"
										id="name"
										name="name"
										value={name}
										onChange={this.onChange}
										placeholder="Name"
									/>
								</div>
								<div className="col-md-6 form-group">
									<input
										type="email"
										className="form-control"
										id="email"
										name="email"
										value={email}
										onChange={this.onChange}
										placeholder="E-mail"
									/>
								</div>
							</div>

							<div className="form-group">
								<input
									type="text"
									className="form-control"
									id="subject"
									name="subject"
									value={subject}
									onChange={this.onChange}
									placeholder="Subject"
								/>
							</div>
							<div className="form-group">
								<textarea
									className="form-control"
									type="text"
									id="message"
									name="message"
									value={message}
									onChange={this.onChange}
									placeholder="Message"
									rows="10"
								/>
							</div>
							<div className id="message-loader" style={{ marginLeft: '45%' }}>
								<div />
								<div />
								<div />
								<div />
								<div />
								<div />
								<div />
								<div />
							</div>
							<div className="row mb-5">
								<div className="col-md-2" />
								<div className="col-md-8 mb-1 mt-5">
									<button className="btn" id="message-submit" type="submit" value="submit">
										<strong>Submit</strong>
									</button>
								</div>
								<div className="col-md-2" />
							</div>
						</form>
					</div>
					<div className="col-md-3" />
				</div>
			</div>
		);
	}
}

export default UserForm;
